﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControll : MonoBehaviour
{
    float h, v;
    public float speed;
    Camera cam;
    Vector3 rotPoint;

    void Start()
    {
        cam = Camera.main;
    }

    int GetCamSpeed(Camera cam)
    {
        float currentY = cam.orthographic ? cam.orthographicSize : cam.transform.position.y;

        if (currentY <= 20) return 10;
        if (currentY <= 100) return 50;
        if (currentY <= 500) return 250;
        else return 500;
    }
    // Update is called once per frame
    void LateUpdate()
    {
        cam.transform.parent.position += cam.transform.parent.right * Input.GetAxis("Horizontal") * GetCamSpeed(cam) * 0.1f;
        cam.transform.parent.position += cam.transform.parent.forward * Input.GetAxis("Vertical") * GetCamSpeed(cam) * 0.1f;

        if (Input.GetAxis("Mouse ScrollWheel") != 0)
        {
            
            
            
                transform.position += Vector3.up * GetCamSpeed(cam) * -Input.GetAxis("Mouse ScrollWheel");
                transform.position = new Vector3(transform.position.x, Mathf.Clamp(transform.position.y, 2, 1000), transform.position.z);
            
        }

        

    }
}
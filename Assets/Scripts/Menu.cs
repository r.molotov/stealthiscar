﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu : MonoBehaviour
{
    public void LoadLevel(int lvl)
    {
        GameManager.Instance().LoadLevel(lvl);
    }
    public void ExitGame()
    {
        GameManager.Instance().ExitGame();
    }
}

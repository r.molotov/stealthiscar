﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    float h, v;
    public float speed;
    public Text scoreText;
    public Text endGameText;
    Rigidbody rb;

    public int score = 0;
    public int total = 0;
    private void Start()
    {
        GameManager.Instance().player = this;
        Car[] cars = GameObject.FindObjectsOfType<Car>();
        for (int i = 0; i < cars.Length; i++)
        {
            total += cars[i].cost;

        }
        ChangeScore(0);
        rb = GetComponent<Rigidbody>();
        
    }
    

    void Update()
    {
        h = Input.GetAxis("Horizontal");
        v = Input.GetAxis("Vertical");

        transform.Translate(Vector3.forward*Time.deltaTime*h*speed);
        transform.Translate(Vector3.left * Time.deltaTime * v*speed);
        transform.Rotate(Vector3.up*Input.GetAxis("Mouse X"));
        transform.Rotate(Vector3.up * Input.GetAxis("Mouse Y"));
    }

    public void ChangeScore(int value)
    {
        score += value;
        scoreText.text = "Score: " + score + "/" + total;
        if (score == total)
        {           
            endGameText.enabled = true;
            endGameText.GetComponent<Animator>().Play("textAnim");
            print("WIN");
            GameManager.Instance().playerScore += score;
            GameManager.Instance().LoadLevel(1);
            score = 0;
            scoreText.text = "Score: " + score + "/" + total;
        }
    }
}

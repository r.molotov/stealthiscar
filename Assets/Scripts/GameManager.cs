﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public GameObject HUD;
    public int playerScore = 0;
    public float GameTime;
    float time;
    public Text timerText;
    private static GameManager _instance;
    public List<Transform> wayPoints;
    public PlayerController player;
    public static GameManager Instance() { return _instance; }
    private void Awake()
    {
        if (GameManager.Instance() == null)
            _instance = this;
        DontDestroyOnLoad(this);
        DontDestroyOnLoad(player);
        DontDestroyOnLoad(HUD);
    }
    public void AddTime(float value)
    {
        time += value;
    }
    private void Start()
    {
        time = GameTime;
        LoadLevel(1);
    }
    public void LoadMap(Transform ground)
    {
        foreach (Transform point in ground)
        {
            wayPoints.Add(point);
        }
    }


    void Update()
    {
        if (SceneManager.GetActiveScene().buildIndex < 2)
            return;
        time -= Time.deltaTime;
        timerText.text = ((int)(time / 60)).ToString("00") + " : " + (time % 60).ToString("00");
        if (time <= 0)
        {
            LoadLevel(1);
            playerScore = 0;
        }
    }
    public void LoadLevel(int index)
    {
        if (index >= SceneManager.sceneCountInBuildSettings)
        {
            SceneManager.LoadScene(1);
        }
        else
        {
            SceneManager.LoadScene(index);
        }
    }

    public void ExitGame()
    {
        Application.Quit();
        print("good bye");
    }
    void OnLevelWasLoaded(int level)
    {
        time = GameTime;
        wayPoints.Clear();
        // 0 - preload
        // 1 - main menu
        // 2+ - game levels
        if (level >= 2)
        {
            HUD.SetActive(true);
            player.gameObject.SetActive(true);
        }
        else
        {
            HUD.SetActive(false);
            player.gameObject.SetActive(false);
        }
        Cursor.visible = level < 2;
    }
}
